import React from 'react';
import { Header } from './Header';
import { HomePage } from './HomePage';
/** @jsxImportSource @emotion/react */
import { css } from '@emotion/react';
import { fontFamily, fontSize, gray2 } from './Styles';

function App() {
    return (
        <div css={css`
            font-family: ${fontFamily};
            font-size: ${fontSize};
            color: ${gray2};
        `}>
            <Header />
            <HomePage />
        </div>
    );
}

/* <header className="App-header">
                <ProblemComponent />
                <img src={logo} className="App-logo" alt="logo" />
                <p>
                    Edit <code>src/App.tsx</code> and save to reload.
                </p>
                <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
                    Learn React
                </a>
            </header> */

/* class ProblemComponent extends React.Component {
    render() {
        return <div ref="div" />;
    }
} */

export default App;
